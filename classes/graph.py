class Graph:

    def __init__(self, V):
        self.V = V
        self.adj = [[] for i in range(V)]
        self.paths = 0

    def addEdge(self, u, v):
        # Add v to u’s list.
        self.adj[u].append(v)

    # Returns count of paths from 's' to 'd'
    def countPaths(self, s, d):

        self.count(s, d)
        return self.paths

    def count(self, u, d):

        # If current vertex is same as
        # destination, then increment count
        if (u == d):
            self.paths += 1
            print(self.paths)
        # If current vertex is not destination
        else:
            for i in range(0, len(self.adj[u])):
                self.count(self.adj[u][i], d)

# This code is contributed by PranchalK
