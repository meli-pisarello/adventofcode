from read_file import read
from test import toBe
from copy import deepcopy

def neighbours(m, i, j):
    return [m[x][y] for x in [i-1, i, i+1] for y in [j-1, j, j+1] if x in range(0, len(m)) and y in range(0, len(m[x])) and (x, y) != (i, j)]


def directions(m, i, j):
    array = []
    for x in [i-1, i, i+1]:
        for y in [j-1, j, j+1]:
            if x in range(0, len(m)) and y in range(0, len(m[x])) and (x, y) != (i, j):
                array.append(m[x][y])
    return array


def updateSeats(array):
    change = False
    newArray = deepcopy(array)

    for i in range(0, len(array)):
        for j in range(0, len(array[i])):
            myNeighbours = neighbours(array, i, j)
            seat = array[i][j]
            if (seat == 'L' and '#' not in myNeighbours):
                newArray[i][j] = '#'
                change = True
            elif (seat == '#' and myNeighbours.count('#') >= 4):
                newArray[i][j] = 'L'
                change = True
    return (newArray, change)


def updateSeats2(array):
    change = False
    newArray = deepcopy(array)

    for i in range(0, len(array)):
        for j in range(0, len(array[i])):
            neighbours =
    return (newArray, change)

def seatsLayout(array):
    while True:
        array, change = updateSeats(array)
        if not change:
            return countOcuppied(array)

def seatsLayout2(array):
    while True:
        array, change = updateSeats2(array)
        if not change:
            return countOcuppied(array)

def countOcuppied(array):
    occupied = 0
    for row in array:
        occupied += row.count('#')
    return occupied

print('\n********* TEST *********\n')

array = read('./inputs/day11-example.txt')
array = [list(i) for i in array]

if array:
    pass
    toBe('PUZZLE 1', seatsLayout(array), 37)
    toBe('PUZZLE 2', seatsLayout2(array), 26)

print('\n******* SOLUTION *******\n')

array = read('./inputs/day11.txt')
array = [list(i) for i in array]

if array:
    pass
    print(f'PUZZLE 1: {seatsLayout(array)}')
    #print(f'PUZZLE 2: {findWeekness(array, 25)}')
