from read_file import readStr
from test import toBe
import re

def checkFields():
    valid = 0
    passports = string.split('\n\n')

    fields = ('byr:','iyr:','eyr:','hgt:','hcl:','ecl:','pid:')

    for passport in passports:
        if all(field in passport for field in fields):
            valid += 1
    return valid

def checkFieldsAndValues():
    valid = 0
    passports = string.split('\n\n')

    for passport in passports:
        if validate(passport):
            valid += 1
    return valid

def validate(passport):
    try:
        passport = passport.replace('\n', ' ').strip().split(' ')
        data = dict(i.split(':') for i in passport)
        x = data.get('byr', False)
        if not year(x, 1920, 2002):
            return False

        x = data.get('iyr', False)
        if not year(x, 2010, 2020):
            return False

        x = data.get('eyr', False)
        if not year(x, 2020, 2030):
            return False

        x = data.get('hgt', False)
        if not hgt(x):
            return False

        x = data.get('hcl', False)
        if not x or not re.search(r'^#(?:[0-9a-fA-F]{6})$', x):
            return False

        x = data.get('ecl', False)
        if not x or (not x in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']):
            return False

        x = data.get('pid', False)
        if not x or not re.search(r'^\d{9}$', x):
            return False

        return True
    except Exception as e:
        print(f"Something went wrong: {e}")
        return False


def hgt(x):
    if x:
        match = re.search(r'\d+(?=cm|in)', x)
        if match:
            number = int(match.group())

            if number:
                if x.find('cm') > -1:
                    return number >= 150 and number <= 193
                else:
                    return number >= 59 and number <= 76
    return False

def year(x, min, max):
    return x and int(x) >= min and int(x) <= max

# TEST
print('\n********* TEST *********\n')

string = readStr('./inputs/day4-example.txt')
if string:
    toBe('PUZZLE 1', checkFields(), 2)

string = readStr('./inputs/day4-example2.txt')
if string:
    toBe('PUZZLE 2', checkFieldsAndValues(), 4)

# SOLUTION
print('\n******* SOLUTION *******\n')

string = readStr('./inputs/day4.txt')
if string:
    print(f'PUZZLE 1: {checkFields()}')
    print(f'PUZZLE 2: {checkFieldsAndValues()}')
