from read_file import read
from test import toBe

def countTrees(right, down):
    trees = 0
    lines = len(array)
    if (lines):
        length = len(array[0])
        pos = 0
        for i in range(0, lines, down):
            line = array[i]
            if (pos >= length):
                  pos = pos - length
            if line[pos] == '#':
                  trees += 1
            pos += right
    return trees

# TEST
print('\n********* TEST *********\n')

array = read('./inputs/day3-example.txt')
if array:
    count = countTrees(3, 1)
    toBe('PUZZLE 1', count, 7)

    count *= countTrees(1, 1)
    count *= countTrees(5, 1)
    count *= countTrees(7, 1)
    count *= countTrees(1, 2)

    toBe('PUZZLE 2', count, 336)

# SOLUTION
print('\n******* SOLUTION *******\n')

array = read('./inputs/day3.txt')
if array:
    count = countTrees(3, 1)
    print(f'PUZZLE 1: {count}')

    count *= countTrees(1, 1)
    count *= countTrees(5, 1)
    count *= countTrees(7, 1)
    count *= countTrees(1, 2)

    print(f'PUZZLE 2: {count}')
