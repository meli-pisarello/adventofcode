from math import gcd

def compute_lcm(a):
    lcm = a[0]
    for i in a[1:]:
        lcm = lcm * i // gcd(lcm, i)
    return lcm

def part_two(ids):
    buses = []
    for bus in ids:
        if bus != 'x':
            buses.append(int(bus))
        else:
            buses.append('x')
    timestamp = 0
    matched_buses = [buses[0]]
    while True:
        timestamp += compute_lcm(matched_buses)
        print(timestamp)
        for i, bus in enumerate(buses):
            if bus != 'x':
                if (timestamp + i) % bus == 0:
                    if bus not in matched_buses:
                        matched_buses.append(bus)
        if len(matched_buses) == len(buses) - buses.count('x'):
            break

    return timestamp


inp = ['13', 'x', 'x', 'x', 'x', 'x', 'x', '37', 'x', 'x', 'x', 'x', 'x', '461', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', '17', 'x', 'x', 'x', 'x', '19', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', '29', 'x', '739', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', '41', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', '23']
print(part_two(inp))
