from read_file import readStr
from test import toBe

def getGroups(string):
   return string.split('\n\n')

def getAnswersCount(array):
    return [len(set(i.replace('\n', '').strip())) for i in array]

def getCommonAnswers(string):
    answers = string.strip().split('\n')
    common = answers[0]
    for i in range(1, len(answers)):
        common = set(common).intersection(answers[i])
    return common

def getCommonAnswersCount(array):
    return [len(getCommonAnswers(i)) for i in array]

def sumAnswers(string):
    groups = getGroups(string)
    answers = getAnswersCount(groups)
    return sum(answers)

def sumCommonAnswers(string):
    groups = getGroups(string)
    answers = getCommonAnswersCount(groups)
    return sum(answers)

print('\n********* TEST *********\n')

string = readStr('./inputs/day6-example.txt')
if string:
    toBe('PUZZLE 1', sumAnswers(string), 11)
    toBe('PUZZLE 2', sumCommonAnswers(string), 6)


print('\n******* SOLUTION *******\n')

string = readStr('./inputs/day6.txt')
if string:
    print(f'PUZZLE 1: {sumAnswers(string)}')
    print(f'PUZZLE 2: {sumCommonAnswers(string)}')


