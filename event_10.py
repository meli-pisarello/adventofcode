from read_file import read
from test import toBe
from classes.graph import Graph

def findDifference(array):
    difJolt1 = 0
    difJolt3 = 1
    outlet = 0
    maxJolt = max(array)
    while (outlet < maxJolt):
        if (outlet + 1) in array:
            difJolt1 += 1
            outlet += 1
        elif (outlet + 2) in array:
            outlet += 2
        elif (outlet + 3) in array:
            difJolt3 += 1
            outlet += 3
        else:
            break

    return difJolt3 * difJolt1


def getPaths(array):
    array.append(0)
    goal = max(array)
    array.sort()
    g = Graph(len(array))

    for n in range(0, len(array)):
        value = array[n]
        if (value + 1) in array:
            g.addEdge(n, array.index(value+1))
        if (value + 2) in array:
            g.addEdge(n, array.index(value+2))
        if (value + 3) in array:
            g.addEdge(n, array.index(value+3))

    return g.countPaths(0, array.index(goal))

print('\n********* TEST *********\n')

array = read('./inputs/day10-example.txt')
array = [int(i) for i in array]

if array:
    toBe('PUZZLE 1', findDifference(array), 220)
    toBe('PUZZLE 2', getPaths(array), 19208)

print('\n******* SOLUTION *******\n')

array = read('./inputs/day10.txt')
array = [int(i) for i in array]

if array:
    pass
    print(f'PUZZLE 1: {findDifference(array)}')
    print(f'PUZZLE 2: {getPaths(array)}')
