from read_file import read
from test import toBe

def valid(arr, value):
    for i in arr:
        for j in arr:
            if i+j == value:
                if arr.index(i) != arr.index(j):
                    return True
    return False

def findInvalidNumber(array, preamble):
    for i in range(preamble, len(array)):
        if (not valid(array[i-preamble:i], array[i])):
            return array[i]
    return -1


def findWeekness(array, preamble):
    invalid = findInvalidNumber(array, preamble)
    length = len(array)
    for i in range(0, length):
        acc = 0
        contiguos = []
        j = i
        while (acc < invalid and j < length):
            value = array[j]
            acc += value
            contiguos.append(value)
            j += 1
        else:
            if (acc == invalid):
                return (min(contiguos) + max(contiguos))

    return -1

print('\n********* TEST *********\n')

array = read('./inputs/day9-example.txt')
array = [int(i) for i in array]

if array:
    toBe('PUZZLE 1', findInvalidNumber(array, 5), 127)
    toBe('PUZZLE 2', findWeekness(array, 5), 62)


print('\n******* SOLUTION *******\n')

array = read('./inputs/day9.txt')
array = [int(i) for i in array]

if array:
    print(f'PUZZLE 1: {findInvalidNumber(array, 25)}')
    print(f'PUZZLE 2: {findWeekness(array, 25)}')
