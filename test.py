def toBe(testName, received, espected):
    result(testName, received == espected)

def notToBe(testName, received, espected):
    result(testName, received != espected)

def result(testName, condition):
    print(testName, end=": ")
    print('PASS') if condition else print('FAIL')
