from read_file import read
from test import toBe
from copy import deepcopy

def accumulate(array):
    acc = 0
    i = 0
    passed = []
    while i < len(array):
        instruction, value = array[i].split(' ')
        if (instruction == 'nop'):
            i += 1
        elif (instruction == 'acc'):
            acc += int(value)
            i += 1
        elif (instruction == 'jmp'):
            i += int(value)

        if (i not in passed):
            passed.append(i)
        else:
            break
    return (acc, i)


def fix(array):
    acc, i = accumulate(array)
    if (i != len(array)):
        for n in range(0, len(array)):
            if ('jmp' in array[n]):
                newArray = deepcopy(array)
                newArray[n] = array[n].replace('jmp', 'nop')
                acc, i = accumulate(newArray)
                if (i == len(array)):
                    return acc
            elif ('nop' in array[n]):
                newArray = deepcopy(array)
                newArray[n] = array[n].replace('nop', 'jmp')
                acc, i = accumulate(newArray)
                if (i == len(array)):
                    return acc
    return acc

print('\n********* TEST *********\n')

array = read('./inputs/day8-example.txt')
if array:
    toBe('PUZZLE 1', accumulate(array)[0], 5)
    toBe('PUZZLE 2', fix(array), 8)


print('\n******* SOLUTION *******\n')

array = read('./inputs/day8.txt')
if array:
    print(f'PUZZLE 1: {accumulate(array)[0]}')
    print(f'PUZZLE 2: {fix(array)}')


