from read_file import read
from test import toBe

def getRules(array):
    rules = [i.replace('.', '').split('contain') for i in array]
    rules = dict((i[0].strip(), i[1].strip().split(', ')) for i in rules)
    return rules

def findRule(rule, rules, bags = set()):
    for key, values in rules.items():
        if any(v for v in values if rule in v):
            bags.add(key.replace(' bags', ''))
    print(bags)
    print(rule in bags)

    if len(bags) > 0 or rule not in bags:
        for bag in bags:
            outerBags = findRule(bag, rules)
            return bags.union(outerBags)
    else:
        return bags

def posibleBags(array):
    rules = getRules(array)
    bags = findRule('shiny gold', rules)
    print(bags)
    return len(bags)

print('\n********* TEST *********\n')

array = read('./inputs/day7-example.txt')


if array:
    pass
    toBe('PUZZLE 1', posibleBags(array), 4)
    #toBe('PUZZLE 2', sumCommonAnswers(string), 6)


print('\n******* SOLUTION *******\n')

array = read('./inputs/day7.txt')
if array:
    pass
    print(f'PUZZLE 1: {posibleBags(array)}')
    #print(f'PUZZLE 2: {sumCommonAnswers(string)}')


