import re
from read_file import read

array = read('./inputs/day2.txt')
if array:
    # PUZZLE 1
    valid = 0

    for password in array:
        string = password.split(':')
        word = string[1].strip()
        letter = string[0][-1]
        limits = re.findall('[0-9]+', string[0])
        min = int(limits[0])
        max = int(limits[1])

        count = word.count(letter)
        if count >= min and count <= max:
            valid += 1
    else:
        print(f'PUZZLE 1: {valid}')

    # PUZZLE 2

    valid = 0

    for password in array:
        string = password.split(':')
        word = string[1].strip()
        letter = string[0][-1]
        positions = re.findall('[0-9]+', string[0])
        pos1 = int(positions[0]) - 1
        pos2 = int(positions[1]) - 1

        length = len(word)

        check1 = length > pos1 and word[pos1] == letter
        check2 = length > pos2 and word[pos2] == letter

        if (check1 ^ check2):
            valid += 1

    else:
        print(f'PUZZLE 2: {valid}')
