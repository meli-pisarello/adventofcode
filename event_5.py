from read_file import read
from test import toBe

def getSeatID(string):
    row = binarySearch(127, 'F', string[0:7])
    column = binarySearch(7, 'L', string[7:10])

    return (row * 8) + column


def binarySearch(end, lowLetter, string):
    start = 0
    for i in string:
        mid = (start + end) // 2
        if i == lowLetter:
            end = mid
        else:
            start = mid + 1
    return start

print('\n********* TEST *********\n')

array = read('./inputs/day5-example.txt')
if array:
    toBe('PUZZLE 1 - test 1', getSeatID(array[0]), 357)
    toBe('PUZZLE 1 - test 2', getSeatID(array[1]), 567)
    toBe('PUZZLE 1 - test 3', getSeatID(array[2]), 119)
    toBe('PUZZLE 1 - test 4', getSeatID(array[3]), 820)

print('\n******* SOLUTION *******\n')

array = read('./inputs/day5.txt')
if array:
    seats = list(getSeatID(i) for i in array)
    seats.sort()
    print(f'PUZZLE 1: {seats[-1]}')

    for i in range(len(seats) - 1):
        if seats[i+1] - seats[i] == 2:
            print(f'PUZZLE 2: {seats[i] + 1}')
            break


