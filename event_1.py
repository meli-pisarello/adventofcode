from read_file import read

def findTwo(arr):
    for i in arr:
        for j in arr:
            if i+j == 2020:
                if arr.index(i) != arr.index(j):
                    print(f'{i} + {j} = {i+j}')
                    print(f'{i} * {j} = {i*j}')
                    return

def findThree(arr):
    for i in arr:
        for j in arr:
            for k in arr:
                if i+j+k == 2020:
                    i1, i2, i3 = arr.index(i), arr.index(j), arr.index(k)
                    if (i1 != i2) and (i1 != i3) and (i2 != i3):
                        print(f'{i} + {j} + {k} = {i+j+k}')
                        print(f'{i} * {j} * {k} = {i*j*k}')
                        return


array = read('./inputs/day1.txt')
if array:
    array = [int(i) for i in array]
    print('**** PUZZLE 1 ****', end="\n\n")
    findTwo(array)

    print('\n**** PUZZLE 2 ****', end="\n\n")
    findThree(array)
    print()
