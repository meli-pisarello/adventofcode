def read(path):
    try:
        file = open(path, "r")  # opens the file in read mode
        items = file.read().splitlines() # puts the file into an array
        file.close()
        return items
    except FileNotFoundError:
        print("File not found")
    except :
        print("Something went wrong")


def readStr(path):
    try:
        file = open(path, "r")  # opens the file in read mode
        items = file.read()
        file.close()
        return items
    except FileNotFoundError:
        print("File not found")
    except:
        print("Something went wrong")
